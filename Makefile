.PHONY: clean build run

clean:
	rm -rf dist/*.pyz

build: clean
	pipenv run python -m zipapp -m "data_gen.main:main" -p "/usr/bin/env python3" -o ./dist/data_gen.pyz src

run: build
	pipenv run python dist/data_gen.pyz
