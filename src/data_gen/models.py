from datetime import datetime
from uuid import uuid4

from faker import Faker
from faker.providers import python, misc, geo

fake = Faker()
fake.add_provider(geo)
fake.add_provider(misc)
fake.add_provider(python)


class BaseModel:
    _add_created = True
    _attributes = []

    kind = ""

    def _add_attr(self, name, method, *args, **kwargs):
        def gattr():
            data = getattr(fake, method)(*args, **kwargs)
            if method == "coordinate":
                data = str(data)
            return data
        setattr(self, name, gattr)

    def _get_val(self, name):
        val = getattr(self, name)
        if callable(val):
            val = val()
        return val

    def __init__(self):
        self._attrs = ["id", "kind"]
        self.id = str(uuid4())
        if self._add_created:
            self._attrs.append("created_at")
            self.created_at = lambda: datetime.utcnow().isoformat()
        for name, method, args, kwargs in self._attributes:
            self._attrs.append(name)
            self._add_attr(name, method, *args, **kwargs)

    def __iter__(self):
        for name in self._attrs:
            yield name, self._get_val(name)

    def __repr__(self):
        r = "<{}(".format(self.__class__.__name__)
        for i, name in enumerate(self._attrs):
            if i != 0:
                r += ", "
            r += "{}={}".format(name, repr(self._get_val(name)))
        r += ")>"
        return r


class PresenceSensor(BaseModel):
    _attributes = BaseModel._attributes + [("value", "pybool", [], dict())]
    kind = "presenceSensor"


class TemperatureSensor(BaseModel):
    _attributes = BaseModel._attributes + [
        ("value", "pyfloat", [], dict(min_value=-5, max_value=30))
    ]
    kind = "temperatureSensor"


class LightSensor(BaseModel):
    _attributes = BaseModel._attributes + [
        ("value", "pyfloat", [], dict(min_value=-5, max_value=30))
    ]
    kind = "brightnessSensor"


class AtmoPressureSensor(BaseModel):
    _attributes = BaseModel._attributes + [
        ("value", "pyfloat", [], dict(min_value=-5, max_value=30))
    ]
    kind = "atmosphericPressureSensor"


class HumiditySensor(BaseModel):
    _attributes = BaseModel._attributes + [
        ("value", "pyfloat", [], dict(min_value=0, max_value=100))
    ]
    kind = "humiditySensor"


class SoundLevelSensor(BaseModel):
    _attributes = BaseModel._attributes + [
        ("value", "pyfloat", [], dict(min_value=0, max_value=100))
    ]
    kind = "soundLevelSensor"


class GPSSensor(BaseModel):
    _attributes = BaseModel._attributes + [("value", "coordinate", [], dict())]
    kind = "gpsSensor"


class CO2Sensor(BaseModel):
    _attributes = BaseModel._attributes + [
        ("value", "pyfloat", [], dict(min_value=0, max_value=100))
    ]
    kind = "co2Sensor"
