import requests


class Sender:
    def __init__(self, base_url):
        self._base_url = base_url
        self._headers = {}
        self._session = requests.Session()

    def auth(self, login="", pswd=""):
        req = self._session.post(
            self._base_url + "/post", data={"username": login, "password": pswd}
        )
        req.raise_for_status()
        resp = req.json()
        self._headers.update(resp)

    def create_device(self, data={}, headers={}):
        headers.update(self._headers)
        req = self._session.post(
            self._base_url + "/device",
            json=data,
            headers=headers,
        )
        req.raise_for_status()


    def publish(self, device_id, data={}, headers={}):
        headers.update(self._headers)
        req = self._session.post(
            self._base_url + "/device/{}/telemetry".format(device_id),
            json=data,
            headers=headers,
        )
        req.raise_for_status()
