import time
from random import choice

from .models import (
    PresenceSensor,
    TemperatureSensor,
    LightSensor,
    AtmoPressureSensor,
    HumiditySensor,
    SoundLevelSensor,
    GPSSensor,
    CO2Sensor,
)
from .sender import Sender


def chunk_it(seq, num):
    avg = len(seq) / float(num)
    out = []
    last = 0.0

    while last < len(seq):
        out.append(seq[int(last):int(last + avg)])
        last += avg

    return out


def create_sensors(qty=50, sensors=[]):
    res = {}
    for _ in range(qty):
        sensor = choice(sensors)
        sensor = sensor()
        res[sensor.id] = sensor
    return res


def sensors_send(sender, all_sensors={}):
    for uid in all_sensors.keys():
        sensor = dict(all_sensors[uid])
        data = {
            "metricDate": sensor["created_at"],
            "deviceType": sensor["kind"],
            "metricValue": sensor["value"],
        }
        sender.publish(uid, data=data)


def main():
    all_sensors = create_sensors(
        qty=20,
        sensors=[
            PresenceSensor,
            TemperatureSensor,
            LightSensor,
            AtmoPressureSensor,
            HumiditySensor,
            SoundLevelSensor,
            GPSSensor,
            CO2Sensor,
        ]
    )
    print(all_sensors)
    sender = Sender("http://devices.atlantis.com")
    for uid, sensor in all_sensors.items():
        sender.create_device(data={
            "id": sensor.id,
            "name": "[{}/{}]".format(sensor.id, sensor.kind),
            "deviceType": sensor.kind,
        })
    while True:
        sensors_send(sender, all_sensors)
        time.sleep(1)


if __name__ == "__main__":
    main()
